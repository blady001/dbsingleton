/**
 * Created by macblady on 05.12.2017.
 */
public class DbResolver {
    private DBConfig dbConfig;
//    private String dbIdentifier;

    public DbResolver(DBConfig config) {
        this.dbConfig = config;
//        String[] urlParts = config.getUrl().split(":");
//        this.dbIdentifier = urlParts[1];
    }

    public CallableDb getDbManager() throws DbConnectorResolvingException {
        CallableDb manager;
        switch(this.dbConfig.getDbType()) {
            case "sqlite":
                manager = SqliteDbManager.getInstance(this.dbConfig.getUrl());
                break;
            case "mysql":
                manager = MySqlDbManager.getInstance(this.dbConfig.getUrl());
                break;
            case "postgresql":
                manager = new PgToCallableAdapter(new PgSqlIncompatibleImpl());
                break;
            default:
                throw new DbConnectorResolvingException("Couldn't find suitable connector!");
        }
        return manager;
    }



    public static class DbConnectorResolvingException extends Exception {
        public DbConnectorResolvingException(String message) {
            super(message);
        }
    }

}
