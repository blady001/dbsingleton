/**
 * Created by macblady on 13.11.2017.
 */
public class MySqlDbManager extends AbstractDbManager {
    // http://www.freesqldatabase.com/account/
    /*
    Host: sql11.freesqldatabase.com
    Database name: sql11204841
    Database user: sql11204841
    Database password: VzAY72bh3m
    Port number: 3306

    CREATE TABLE `sql11204841`.`tab_student` ( `id` INT NOT NULL, `name` TEXT NOT NULL ) ENGINE = InnoDB;
     */

    private static MySqlDbManager instance = null;
//    private static final String DB_URL = "jdbc:mysql://sql11.freesqldatabase.com:3306/sql11204841?user=sql11204841&password=VzAY72bh3m";

    private MySqlDbManager() {

    }

//    public static synchronized MySqlDbManager getInstance() {
//        if (instance == null) {
//            instance = new MySqlDbManager();
//            System.out.println("Initialized db handler!");
//        }
//        return instance;
//    }

    public static synchronized CallableDb getInstance(String url) {
        if (instance == null) {
            instance = new MySqlDbManager();
            System.out.println("Initialized db handler!");
        }
        instance.setUrl(url);
        return instance;
    }
}
