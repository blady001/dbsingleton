import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by macblady on 13.11.2017.
 */
public abstract class AbstractDbManager implements CallableDb {

    private String url;

    private Record processRow(ResultSet input) throws SQLException {
        Map<String, Object> result = new HashMap<String, Object>();
        List<String> columns = new ArrayList<String>();

        ResultSetMetaData rsmd = input.getMetaData();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            String name = rsmd.getColumnName(i);
            columns.add(name);
        }

        for (String key : columns) {
            Object value = input.getObject(key);
            result.put(key, value);
        }
        return new Record(result);
    }

    private List<Record> collectResults(ResultSet rs) throws SQLException {
        List<Record> results = new ArrayList<Record>();
        while (rs.next()) {
            results.add(this.processRow(rs));
        }
        return results;
    }

    private List<Record> executeFetchSql(String sql) throws SQLException{
        Connection connection = null;
        List<Record> results = null;

        try {
            connection = DriverManager.getConnection(this.getDbUrl());
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);
            ResultSet resultSet = statement.executeQuery(sql);
            results = this.collectResults(resultSet);
        }
        finally {
            if (connection != null)
                connection.close();
        }
        return results == null ? new ArrayList<Record>() : results;
    }

    private void performInsert(String sql) throws SQLException {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(this.getDbUrl());
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);
            statement.executeUpdate(sql);
        } finally {
            if (connection != null)
                connection.close();
        }
    }

    public Record getOne(String tableName, String whereClause) throws SQLException {
        String sql = "SELECT * FROM " + tableName + " WHERE " + whereClause + ";";
        List<Record> r = this.executeFetchSql(sql);
        return r.size() > 0 ? r.get(0) : null;
    }

    public List<Record> getAll(String tableName) throws SQLException {
        String sql = "SELECT * FROM " + tableName + ";";
        return this.executeFetchSql(sql);
    }

    public void insert(String statement) throws SQLException {
        this.performInsert(statement);
    }

    public void delete(String statement) throws SQLException {
        this.performInsert(statement);
    }

    public void update(String statement) throws SQLException {
        this.performInsert(statement);
    }

//    public abstract CallableDb getInstance(String url);

    private String getDbUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
