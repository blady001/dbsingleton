import java.sql.SQLException;
import java.util.List;

/**
 * Created by macblady on 20.11.2017.
 */
public class PgToCallableAdapter implements CallableDb {
    private PgSqlIncompatible pgSqlConnector;

    public PgToCallableAdapter(PgSqlIncompatible sqlConnector) {
        this.pgSqlConnector = sqlConnector;
    }

    public Record getOne(String tableName, String whereClause) throws SQLException {
        return this.pgSqlConnector.getOneFromPg(tableName, whereClause);
    }

    public List<Record> getAll(String tableName) throws SQLException {
        return this.pgSqlConnector.getAllFromPg(tableName);
    }

    public void insert(String sql) throws SQLException {
        this.pgSqlConnector.insertToPg(sql);
    }

    public void delete(String sql) throws SQLException {
        this.pgSqlConnector.deleteFromPg(sql);
    }

    public void update(String sql) throws SQLException {
        this.pgSqlConnector.updateInPg(sql);
    }
}
